
# coding: utf-8

# In[1]:


import re
import pandas as pd


# In[2]:


with open('100.txt') as fout:
    file = fout.readlines()


# In[3]:


parts = [
    r'(?P<host>\S+)',                   # host %h
    r'\S+',                             # indent %l (unused)
    r'(?P<user>\S+)',                   # user %u
    r'\[(?P<time>.+)\]',                # time %t
    r'"(?P<request>.*)"',               # request "%r"
    r'(?P<status>[0-9]+)',              # status %>s
    r'(?P<size>\S+)',                   # size %b (careful, can be '-')
    r'"(?P<referrer>.*)"',              # referrer "%{Referer}i"
    r'"(?P<agent>.*)"',                 # user agent "%{User-agent}i"
]
pattern = re.compile(r'\s+'.join(parts)+r'\s*\Z')


# In[4]:


ip, vol = [], []
for line in file:
    log_data = pattern.match(line).groupdict()
    vol.append(int(log_data['size']))
    ip.append(log_data['host'])
    


# In[5]:


test_data = pd.DataFrame({'ip':ip, 'vol':vol, 'count':[1 for i in range(len(vol))]})
test_data.head()


# In[6]:


grouped = test_data.groupby('ip')[['vol', 'count']].sum()
grouped['avg'] = pd.Series(grouped['vol']//grouped['count'], index=grouped.index)

