#!/usr/bin python
"""combiner.py"""

from operator import itemgetter
import sys

current_word = None
current_val = 0
current_count = 0
word = None

# вход с stdin
for line in sys.stdin:
    #очистка строки
    line = line.strip()
    word, count, vl = line.split(',', 2)

    try:
        count = int(count)
        vl = int(vl)
    except ValueError:
        continue

    if current_word == word:
        current_val += count
        current_count += vl
    else:
        if current_word:
            #выдаём результат в stdout
            print '%s,%s,%s' % (current_word, current_val, current_val/current_count)
        current_val = count
        current_count = vl
        current_word = word

if current_word == word:
    #выдаём результат в stdout
    print '%s,%s,%s' % (current_word, current_val, current_val/current_count)