#!/usr/bin python
"""reducer.py"""

import sys

current_word = None
current_val = 0
current_avg = 0
word = None

# вход с stdin
for line in sys.stdin:
    #очистка строки
    line = line.strip()
    word, count, vl = line.split(',', 2)
    #Перевод строки в int
    try:
        count = int(count)
        vl = int(vl)
    except ValueError:
        continue

    if current_word == word:
        current_val += count
        current_avg += vl
    else:
        if current_word:
            # Вывод в stdout
            print '%s,%s,%s' % (current_word, current_avg, current_val)
        current_val = count
        current_avg = vl
        current_word = word

if current_word == word:
    # Вывод в stdout
    print '%s,%s,%s' % (current_word, current_avg, current_val)